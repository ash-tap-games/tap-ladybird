/****************************************************************************
 Copyright (c) 2010 cocos2d-x.org

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import "AppController.h"
#import "platform/ios/CCEAGLView-ios.h"
#import "cocos2d.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import <UIKit/UIKit.h>
#import "AppController.h"
#import "cocos2d.h"
//#import "CCEAGLView.h"
#import "InappGlobal.h"
#import "RootViewController.h"
#import <Social/Social.h>
#import <Chartboost/Chartboost.h>
#import <CommonCrypto/CommonDigest.h>
#import <AdSupport/AdSupport.h>
#import "SVProgressHUD.h"

@implementation AppController

#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
//static AppDelegate s_sharedApplication;

static AppDelegate s_sharedApplication;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [GADMobileAds configureWithApplicationID:@"ca-app-pub-1605861861953364~8444212177"];

    cocos2d::Application *app = cocos2d::Application::getInstance();
    app->initGLContextAttrs();
    cocos2d::GLViewImpl::convertAttrs();

    // Override point for customization after application launch.

    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];

    // Init the CCEAGLView
    CCEAGLView *eaglView = [CCEAGLView viewWithFrame: [window bounds]
                                         pixelFormat: (NSString*)cocos2d::GLViewImpl::_pixelFormat
                                         depthFormat: cocos2d::GLViewImpl::_depthFormat
                                  preserveBackbuffer: NO
                                          sharegroup: nil
                                       multiSampling: NO
                                     numberOfSamples: 0 ];
    
    // Enable or disable multiple touches
    [eaglView setMultipleTouchEnabled:NO];

    // Use RootViewController manage CCEAGLView 
    viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    viewController.wantsFullScreenLayout = YES;
    viewController.view = eaglView;

    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:viewController];
    }

    [viewController.view setMultipleTouchEnabled:YES];
    
    BOOL isRunMoreThanOnce = [[NSUserDefaults standardUserDefaults] boolForKey:@"isRunMoreThanOnce"];
    if(!isRunMoreThanOnce)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isRunMoreThanOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
    [Chartboost startWithAppId:CHARTBOOST_ID appSignature:CHARTBOOST_SIG delegate:self];
    
    [window makeKeyAndVisible];

    [[UIApplication sharedApplication] setStatusBarHidden:true];

    // IMPORTANT: Setting the GLView should be done after creating the RootViewController
    cocos2d::GLView *glview = cocos2d::GLViewImpl::createWithEAGLView(eaglView);
    cocos2d::Director::getInstance()->setOpenGLView(glview);

    //app->run();
     [self AdmobBanner2];
    
    cocos2d::Application::getInstance()->run();
    
   

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
     //We don't need to call this method any more. It will interupt user defined game pause&resume logic
    cocos2d::Director::getInstance()->pause();
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
     //We don't need to call this method any more. It will interupt user defined game pause&resume logic
     cocos2d::Director::getInstance()->resume();
     cocos2d::Director::getInstance()->startAnimation();
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    cocos2d::Director::getInstance()->stopAnimation();
    cocos2d::Application::getInstance()->applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    cocos2d::Application::getInstance()->applicationWillEnterForeground();
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
    [window release];
    [super dealloc];
}

-(void)AdmobBanner2
{
    
        [bannerview removeFromSuperview];
        
        bannerview = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        bannerview.delegate=self;
        bannerview.adUnitID=ADMOB_BANNER;
        bannerview.rootViewController=self->viewController;
        [viewController.view addSubview:bannerview];
        
        GADRequest *request = [GADRequest request];
        request.testDevices=@[kDFPSimulatorID];
        [bannerview loadRequest:request];
        
    
}

-(void)AdmobFullScreen
{
    
        AdmobFullScreen=[[GADInterstitial alloc] initWithAdUnitID:ADMOB_FULLSCREEN];
        AdmobFullScreen.delegate=self;
        
        GADRequest *request = [GADRequest request];
        request.testDevices=@[kDFPSimulatorID];
        [AdmobFullScreen loadRequest:request];
    
    
}

#pragma mark ==== Chartboost =====

-(void)ChartboostFullScreen
{
    
        [Chartboost showInterstitial:CBLocationHomeScreen];
    
}

//======= ScreenShots ======

-(void)ScreenShot
{
    //[Chartboost showMoreApps:CBLocationHomeScreen];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@""];
    NSString *URLImg = [documentsDirectory stringByAppendingPathComponent:@"ScreenShot.png"];
    
    //NSString *texttoshare = [NSString stringWithFormat:@"%@ %@",SHARING_TEXT,SHORT_URL]; //this is your text string to share
    UIImage *imagetoshare = [UIImage imageWithContentsOfFile:URLImg]; //this is your image to share
    //NSLog(@"imge is %@",imagetoshare);
    
    //UIImage *imagetoshare1 = [UIImage imageWithData:[NSData dataWithContentsOfFile:URLImg]];
    // NSLog(@"imge is %@",imagetoshare);
    NSArray *activityItems = @[imagetoshare];
    
    if (IS_IPAD)
    {
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
        [viewController presentViewController:activityVC animated:TRUE completion:nil];
        UIPopoverPresentationController *presentationController = [activityVC popoverPresentationController];
        presentationController.sourceView = viewController.view;
    }
    else
    {
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
        [viewController presentViewController:activityVC animated:TRUE completion:nil];
    }
    
}


#pragma mark GameCenter View Controllers
- (void)showLeaderboard
{
    //    CLeaderboardViewController *leaderboardController = [[CLeaderboardViewController alloc] init];
    //    if (leaderboardController != NULL) {
    //        leaderboardController.category = self.currentLeaderBoard;
    //        leaderboardController.timeScope = GKLeaderboardTimeScopeAllTime;
    //        leaderboardController.leaderboardDelegate = self;
    //
    //        UIApplication* clientApp = [UIApplication sharedApplication];
    //        UIWindow* topWindow = [clientApp keyWindow];
    //        if (!topWindow)
    //        {
    //            topWindow = [[clientApp windows] objectAtIndex:0];
    //        }
    //        leaderboardController.leaderboardDelegate = (AppController *)clientApp.delegate;
    //
    //
    //        tempVC=[[UIViewController alloc] init];
    //        if (!topWindow)
    //        {
    //            topWindow = [[clientApp windows] objectAtIndex:0];
    //        }
    //        [topWindow addSubview:tempVC.view];
    //        leaderboardController.leaderboardDelegate = (AppController *)[UIApplication sharedApplication].delegate;
    //        [tempVC presentViewController:leaderboardController animated:YES completion:nil];
    //        
    //    }
    
}

-(void)UnlockLevels
{
    [SVProgressHUD showWithStatus:@"Loading..."];
    [MKStoreManager SharedManager].delegate=self;
   // [[MKStoreManager SharedManager]buyProductIdentifiers:Buy_UnlockAll];
}
-(void)RemoveAdvertise
{
    [SVProgressHUD showWithStatus:@"Loading..."];
    [MKStoreManager SharedManager].delegate=self;
   // [[MKStoreManager SharedManager]buyProductIdentifiers:Buy_RemoveAds];
}
-(void)FullPurchase
{
    [SVProgressHUD showWithStatus:@"Loading..."];
    [MKStoreManager SharedManager].delegate=self;
    //[[MKStoreManager SharedManager]buyProductIdentifiers:Buy_FullPurchase];
}

-(void)RestorePurchase
{
    [SVProgressHUD showWithStatus:@"Loading..."];
    [MKStoreManager SharedManager].delegate=self;
    [[MKStoreManager SharedManager]restorePurchases];
}

-(void)prodctIdentifierPurchaseFailed
{
    //NSLog(@"fail");
    [SVProgressHUD dismiss];
}
-(void)productIdentifierPurchased:(NSString *)productIdentifier
{
    
    
    
    [SVProgressHUD dismiss];
}

-(void)didRestorePurchased
{
    //NSLog(@"restore success");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Restore Successfully..." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
    
    [SVProgressHUD dismiss];
}

-(void)shouldnotPurchased
{
    //NSLog(@"you haven't not bought app");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"you haven't not bought app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [SVProgressHUD dismiss];
}

-(void)didFailedRestorePurchased
{
    [SVProgressHUD dismiss];
}

-(void)adViewDidReceiveAd:(GADBannerView *)BannerView
{
    [UIView commitAnimations];
}

-(void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    [AdmobFullScreen presentFromRootViewController:viewController];
}

-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"error %@",error);
}



@end
