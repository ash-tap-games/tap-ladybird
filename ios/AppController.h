#import <UIKit/UIKit.h>

#import <GoogleMobileAds/GoogleMobileAds.h>
#import <Chartboost/Chartboost.h>
//#import <Chartboost/CBNewsfeed.h>
#import <CommonCrypto/CommonDigest.h>
#import <AdSupport/AdSupport.h>
#import "MKStoreManager.h"


@class RootViewController;



@interface AppController : NSObject <UIAccelerometerDelegate, UIAlertViewDelegate, UITextFieldDelegate,UIApplicationDelegate,GADBannerViewDelegate,GADInterstitialDelegate,MKstoreManagerDelegate,ChartboostDelegate>
{
    UIWindow *window;
    RootViewController    *viewController;
    
    GADBannerView *bannerview;
    GADInterstitial *AdmobFullScreen;
    
    int setHints;
}

//  ====== For Admob =====
-(void)AdmobBanner1;
-(void)AdmobBanner2;
-(void)AdmobFullScreen;

// ===== Chartboost ====
-(void)ChartboostFullScreen;
-(void)ScreenShot;
- (void)showLeaderboard;

// ===== Inap ====

-(void)RemoveAdvertise;
-(void)UnlockLevels;
-(void)FullPurchase;
-(void)RestorePurchase;
@end

