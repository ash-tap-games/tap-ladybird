//
//  MKStoreManager.h
//  Ultimate_SMS
//
//  Created by Lion User on 10/24/13.
//  Copyright (c) 2013 Dreams_Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#include "cocos2d.h"


@protocol MKstoreManagerDelegate

-(void)productIdentifierPurchased:(NSString *)productIdentifier;
-(void)prodctIdentifierPurchaseFailed;
-(void)didRestorePurchased;
-(void)shouldnotPurchased;
-(void)didFailedRestorePurchased;

@end

@interface MKStoreManager : NSObject<SKProductsRequestDelegate,SKPaymentTransactionObserver>
{
    NSSet * _productIdentifiers;
    NSArray * _products;
    NSMutableSet * _purchasedProducts;
    SKProductsRequest * _request;
    id<MKstoreManagerDelegate>delegate;
    
}
@property(nonatomic, retain)NSSet * productIdentifiers;
@property(nonatomic, retain)NSArray * products;
@property(nonatomic, retain)NSMutableSet * purchasedProducts;
@property(nonatomic, retain)SKProductsRequest * request;
@property(nonatomic, retain)id<MKstoreManagerDelegate>delegate;

+(MKStoreManager *)SharedManager;
+(BOOL)isFeaturePurchased:(NSString *)featureID;

-(void)requestProducts;
-(id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
-(void)buyProductIdentifiers:(NSString *)productIdentifiers;
-(void)restorePurchases;


@end
