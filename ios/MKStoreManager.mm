//
//  MKStoreManager.m
//  Ultimate_SMS
//
//  Created by Lion User on 10/24/13.
//  Copyright (c) 2013 Dreams_Technology. All rights reserved.
//

#import "MKStoreManager.h"
#include <StoreKit/StoreKit.h>
#include "InappGlobal.h"

static MKStoreManager *_sharedStoreManager;

@implementation MKStoreManager


@synthesize productIdentifiers = _productIdentifiers;
@synthesize products = _products;
@synthesize purchasedProducts = _purchasedProducts;
@synthesize request = _request;
@synthesize delegate;

+(MKStoreManager *)SharedManager
{
    if (_sharedStoreManager==nil)
    {
        NSSet *productIdentifiers = [NSSet setWithObjects: nil];
        _sharedStoreManager = [[MKStoreManager alloc]initWithProductIdentifiers:productIdentifiers];
        [[SKPaymentQueue defaultQueue]addTransactionObserver:_sharedStoreManager];
    }
    return _sharedStoreManager;
}

+(BOOL)isFeaturePurchased:(NSString *)featureID
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault boolForKey:featureID];
}

-(id)initWithProductIdentifiers:(NSSet *)productIdentifiers
{
    if ((self=[super init])) {
        _productIdentifiers = productIdentifiers;
        NSMutableSet *purchasedProduct = [NSMutableSet set];
        for (NSString *productIdentifier in _productIdentifiers)
        {
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults]boolForKey:productIdentifier];
            if (productPurchased)
            {
                [purchasedProduct addObject:productIdentifiers];
                
            }
            
        }
        self.purchasedProducts=purchasedProduct;
    }
    return self;
}

-(void)requestProducts
{
    self.request=[[SKProductsRequest alloc]initWithProductIdentifiers:_productIdentifiers];
    _request.delegate=self;
    [_request start];
}

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    self.products= response.products;
    self.request=nil;
    //[[NSNotificationCenter defaultCenter]postNotificationName:kProductsLoadedNotification object:_products];
    
}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"The In-App Store is currently unavailable, please try again later.");
}

-(void)recordTransation:(SKPaymentTransaction *)transation
{
    
}

-(void)provideContent:(NSString *)productIdentifier
{
    /*
    if ([productIdentifier isEqualToString:Buy_UnlockAll])
    {
        NSUserDefaults *userdefault3 = [NSUserDefaults standardUserDefaults];
        [userdefault3 setBool:YES forKey:Buy_UnlockAll];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        cocos2d::UserDefault::getInstance()->setBoolForKey("Buy_UnlockAll", YES);
        cocos2d::UserDefault::getInstance()->flush();
    }
    else if ([productIdentifier isEqualToString:Buy_RemoveAds])
    {
        NSUserDefaults *userdefault3 = [NSUserDefaults standardUserDefaults];
        [userdefault3 setBool:YES forKey:Buy_RemoveAds];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        cocos2d::UserDefault::getInstance()->setBoolForKey("Buy_RemoveAds", YES);
        cocos2d::UserDefault::getInstance()->flush();
    }
    else if ([productIdentifier isEqualToString:Buy_FullPurchase])
    {
        NSUserDefaults *userdefault3 = [NSUserDefaults standardUserDefaults];
        [userdefault3 setBool:YES forKey:Buy_FullPurchase];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        cocos2d::UserDefault::getInstance()->setBoolForKey("Buy_Full_Purchase", YES);
        cocos2d::UserDefault::getInstance()->flush();
    }*/
        //[[NSNotificationCenter defaultCenter]postNotificationName:kProductPurchasedNotification object:productIdentifier userInfo:nil];
}

-(void)completetransation:(SKPaymentTransaction *)transation
{
    [self recordTransation:transation];
    NSLog(@"Completed transation");
    [self provideContent:transation.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transation];
    [delegate productIdentifierPurchased:transation.payment.productIdentifier];
}

-(void)restoreTransation:(SKPaymentTransaction *)transation
{
    [self recordTransation:transation];
    [self provideContent:transation.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue]finishTransaction:transation];
}

-(void)failedTransation:(SKPaymentTransaction *)transation
{
    if (transation.error.code != SKErrorPaymentCancelled)
    {
        NSLog(@"Transaction error: %@",
              transation.error.localizedDescription);
         UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Purchase failed!"
         message:transation.error.localizedDescription delegate:nil
         cancelButtonTitle:nil otherButtonTitles:@"OK", nil];

        [alert show];
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transation];
    [delegate prodctIdentifierPurchaseFailed];
    
}

-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transation in transactions)
    {
        switch (transation.transactionState) {
            case SKPaymentTransactionStatePurchased:
                [self completetransation:transation];
                break;
                
            case SKPaymentTransactionStateFailed:
                [self failedTransation:transation];
                break;
                
            case SKPaymentTransactionStateRestored:
                [self recordTransation:transation];
                break;
                
            default:
                break;
        }
    }
}

-(void)buyProductIdentifiers:(NSString *)productIdentifiers
{
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:productIdentifiers];
    [[SKPaymentQueue defaultQueue]addPayment:payment];
}

-(void)restorePurchases
{
    [[SKPaymentQueue defaultQueue]addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue]restoreCompletedTransactions];
}

-(void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    BOOL flag=NO;
    for (SKPaymentTransaction *transation in queue.transactions)
    {
        [self restoreTransation:transation];
        flag=YES;
    }
    if (flag)
    {
        [delegate didRestorePurchased];
    }
    else
    {
        [delegate shouldnotPurchased];
    }
}

-(void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    [delegate didFailedRestorePurchased];
}


-(void)dealloc
{
    _productIdentifiers=nil;
    _products=nil;
    _purchasedProducts=nil;
    _request=nil;
    [super dealloc];

    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    [_request release];
}

@end
