#include "sk_game_services.h"
#include "cocos2d.h"

using namespace cocos2d;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

//void ios_AdmobBanner1();
void ios_AdmobBanner2();
void ios_AdmobFullScreen();
void ios_ChartboostFullScreen();
void ios_ScreenShot();

void ios_UnlockLevels();
void ios_FullPurchase();
void fullPurchase();
void ios_RemoveAds();
void ios_RestorePurchase();

void ios_ShowLeaderboard();

#endif


using namespace cocos2d;

namespace sk
{
    
    namespace game_services
    {
        
        
    }

#pragma mark ===== Advertose ====

    
    void sk::game_services::AdmobBanner_2()
    {
            ios_AdmobBanner2();
    }
    
    void sk::game_services::Admob_FullScreen()
    {
            ios_AdmobFullScreen();
    }
    void sk::game_services::Chartboost_FullScreen()
    {
            ios_ChartboostFullScreen();
    }
    //==== Screen Shot =====//
    
    void sk::game_services::Load_ScreenShot()
    {
        ios_ScreenShot();
    }
    
    void sk::game_services::showLeaderboard()
    {
        ios_ShowLeaderboard();
    }
    
    //==== Inap =====//
    
    void sk::game_services::unlockLevels()
    {
        ios_UnlockLevels();
    }
    
    void sk::game_services::removeAds()
    {
        ios_RemoveAds();
    }
    
    void sk::game_services::fullPurchase()
    {
        ios_FullPurchase();
    }
    
    void sk::game_services::restorePurchase()
    {
        ios_RestorePurchase();
    }

} // namespace game_services

