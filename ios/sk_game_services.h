#pragma once

#ifndef _SK_GAME_SERVICES__
#define _SK_GAME_SERVICES__

#define SK_NO_PLAYHEAVEN    // if enabled, Playheaven is not used
//#define SK_NO_ADMOB         // if enabled, it uses iAD only
//#define SK_NO_ADCOLONY      // if enabled, adcolony ads and TV buttons will not work
//#define SK_DISABLE_IN_APPS  // if enabled, no in-apps will

#include <string>
#include <vector>

namespace cocos2d
{
    class Image;
}

namespace sk
{
    
    namespace game_services
    {

        
        //==== Advertise ====
        void AdmobBanner_1();
        void AdmobBanner_2();
        void Admob_FullScreen();
        void Chartboost_FullScreen();
        void Load_ScreenShot();
        
        void unlockLevels();
        void restorePurchase();
        void removeAds();
        void fullPurchase();
        
        void showLeaderboard();
        
    } // namespace game_servies
    
} // namespace sk




#endif // _SK_GAME_SERVICES__
