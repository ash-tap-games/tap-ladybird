//
//  InappGlobal.h
//  Hidden Objects
//
//  Created by Fireboy on 12/08/14.
//
//

#ifndef Hidden_Objects_InappGlobal_h
#define Hidden_Objects_InappGlobal_h


#define IS_IPHONE ([[[UIDevice currentDevice]model]hasPrefix:@"iPhone"])

#define IS_IPOD ([[[UIDevice currentDevice]model]hasPrefix:@"iPod touch"])

#define IS_HEIGHT_GET_568 [[UIScreen mainScreen]bounds].size.width==568.0f
#define IS_IPHONE_5 (IS_HEIGHT_GET_568)

#define IS_HEIGHT_GET_667 [[UIScreen mainScreen]bounds].size.width==667.0f
#define IS_IPHONE_6 (IS_HEIGHT_GET_667)

#define IS_HEIGHT_GET_736 [[UIScreen mainScreen]bounds].size.width==736.0f
#define IS_IPHONE_6P (IS_HEIGHT_GET_736)

#define IS_IPAD ([[[UIDevice currentDevice]model]hasPrefix:@"iPad"])


#pragma mark ====== Advertisemnet ========

#define CHARTBOOST_ID       @"5ab38e0ef7c1590bbef010d3"
#define CHARTBOOST_SIG      @"d6fe3c6fe18acc9edc127165bfe338e89eae2858"


#define ADMOB_BANNER        @"ca-app-pub-1605861861953364/8288654726"
#define ADMOB_FULLSCREEN    @"ca-app-pub-1605861861953364/7430513407"




#endif
